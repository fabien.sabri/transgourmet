# Deeplink App Mobile Transgourmet

Cette page est en lien avec la page Confluence https://transgourmet.atlassian.net/wiki/spaces/AM/pages/68190612/Deeplink et permet de tester les différents deeplink de l'App.

## Deeplink de domaine

Recette : https://egourmet-rec.transgourmet.fr/egourmet/prospect/vosinfos

Prod : https://webshop.transgourmet.fr/egourmet/prospect/vosinfos

Prod (validation compte) : https://webshop.transgourmet.fr/egourmet/prospect/vosinfos?link=BA4867594850142679133&numberSteps=null&USERNAME=prospect202311200759368574768

## Fallback

### Deeplink non reconnu

tgt://test

tgt://produit/test

Produit non existant : tgt://produit/0123456789

tgt://recherche/test

Erreur sur paramètre de recherche (clé) : tgt://recherche/PARAMETRE_ERRONE=ME,MP,EG

Erreur sur paramètre de recherche (valeur) : tgt://recherche/SEARCH_PRICE_TYPE_ME=VALEUR_ERRONEE

### Problème de casse

tgt://Produit/801746

tgt://Recherche/SEARCH_CATEGORY_ID1=12543&sortOrder=SortPush:ASC

### Problème de droit d'accès (à tester selon l'utilisateur connecté)

tgt://commandes

tgt://mon-compte

## Ecrans de l'App

tgt://home

tgt://mercuriales-cadencier

tgt://my-lists

tgt://recherche

tgt://panier

tgt://notifications

tgt://mon-compte

tgt://commandes

tgt://login

## Produit

tgt://produit/801746

tgt://produit/801753

## Commande

tgt://commande/51507736

tgt://commande/49223497

## Réclamation

tgt://reclamation/L1312217292

## Recherche (titre)

tgt://recherche/

tgt://recherche/titre=Recherche Deeplink

[tgt://recherche/SEARCH_PRICE_TYPE_ME=ME,MP,EG&titre=Recherche Deeplink](tgt://recherche/titre=Recherche Deeplink)

## Recherche (filtres)

Mercuriales : tgt://recherche/SEARCH_PRICE_TYPE_ME=ME,MP,EG

Cadencier : tgt://recherche/SEARCH_SLT_FREQ_PURCHASES=Y

Mercuriales & cadencier : tgt://recherche/SEARCH_MERCURIAL_CADENCIER=Y

Rayons "Fruits et Légumes" / Familles "Fruits rouges", "Fruits a coques", "Melon" : tgt://recherche/SEARCH_NOMENCLATURE_LIST=FPR_0044.0005,FPR_0044.0006,FPR_0044.0007

Labels : tgt://recherche/SEARCH_LABEL=BIO,AOP

Régime alimentaire (Vegan + Veggie) : tgt://recherche/SEARCH_ASSORTMENT=9001,9002

Température : tgt://recherche/SEARCH_TEMPERATURE=TS,TA

Marque : tgt://recherche/SEARCH_BRAND_LIST=BD_DELIFRANCE,BD_BRIDOR,BD_TGT_NATURA

Région : [tgt://recherche/SEARCH_REGION=BRETAGNE,SUD OUEST](tgt://recherche/SEARCH_REGION=BRETAGNE,SUD OUEST)

Avantage prix (exclu web + promo perso + produits à points) : tgt://recherche/SEARCH_PRICE_TYPE_PR=PE,PC&IS_FIDELITY_POINT=Y

Distance du lieu de production : tgt://recherche/SEARCH_PDTS_LOCAUX=100

## Recherche (tris)

De A à Z : tgt://recherche/sortOrder=SortByAlpha:ASC

De Z à A : tgt://recherche/sortOrder=SortByAlpha:DESC

Prix/Pièce croissant : tgt://recherche/sortOrder=SortTgProductPrice:ASC

Prix/Pièce décroissant : tgt://recherche/sortOrder=SortTgProductPrice:DESC

Prix/Kg croissant : tgt://recherche/sortOrder=WT_kg:ASC

Prix/Kg décroissant : tgt://recherche/sortOrder=WT_kg:DESC

Prix/L croissant : tgt://recherche/sortOrder=VLIQ_L:ASC

Prix/L décroissant : tgt://recherche/sortOrder=VLIQ_L:DESC

## Recherche (titre + filtres + tris)

[tgt://recherche/titre=Recherche Deeplink&SEARCH_PRICE_TYPE_ME=ME,MP,EG](tgt://recherche/titre=Recherche Deeplink&SEARCH_PRICE_TYPE_ME=ME,MP,EG)

tgt://recherche/SEARCH_CATEGORY_ID1=12543&sortOrder=SortPush:ASC

tgt://recherche/sortOrder=SortByPricePriority:ASC&SEARCH_CATEGORY_TYPE_ID=PUSH_PROMO&SEARCH_PRICE_TYPE_PR=PN,PR,PE,PC
